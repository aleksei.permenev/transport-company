package ru.ulstu.service.transport.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.ulstu.model.OffsetablePageRequest;
import ru.ulstu.model.PageableItems;
import ru.ulstu.model.transport.company.Transport;
import ru.ulstu.repository.transport.company.TransportRepository;
import ru.ulstu.service.Crud;

import java.util.List;
import java.util.Optional;

@Service
public class TransportCrudRepository implements Crud<Transport> {
    private final TransportRepository transportRepository;

    public TransportCrudRepository(TransportRepository transportRepository) {
        this.transportRepository = transportRepository;
    }

    @Override
    public Transport create(Transport transport) {
        return transportRepository.saveAndFlush(transport);
    }

    @Override
    public List<Transport> findAll() {
        return transportRepository.findAll();
    }

    @Override
    public Transport get(Integer id) {
        return transportRepository.getOne(id);
    }

    @Override
    public Optional<Transport> find(Integer id) {
        return transportRepository.findById(id);
    }

    @Override
    public PageableItems<Transport> findAll(int offset, int count) {
        final Page<Transport> page = transportRepository.findAll(new OffsetablePageRequest(offset, count));
        return new PageableItems<>(page.getTotalElements(), page.getContent());
    }

    @Override
    public Transport update(Transport transport) {
        return transportRepository.saveAndFlush(transport);
    }

    @Override
    public void delete(Transport transport) {
        transportRepository.delete(transport);
    }
}
