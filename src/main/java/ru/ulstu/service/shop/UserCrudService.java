package ru.ulstu.service.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.ulstu.model.OffsetablePageRequest;
import ru.ulstu.model.PageableItems;
import ru.ulstu.model.shop.User;
import ru.ulstu.repository.shop.UserRepository;
import ru.ulstu.service.Crud;

import java.util.List;
import java.util.Optional;

@Service
public class UserCrudService implements Crud<User> {
    private final UserRepository userRepository;

    public UserCrudService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User get(Integer id) {
        return userRepository.getOne(id);
    }

    @Override
    public Optional<User> find(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    public PageableItems<User> findAll(int offset, int count) {
        final Page<User> page = userRepository.findAll(new OffsetablePageRequest(offset, count));
        return new PageableItems<>(page.getTotalElements(), page.getContent());
    }

    @Override
    public User update(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }
}
