package ru.ulstu;

import org.hibernate.sql.InsertSelect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import ru.ulstu.model.shop.Cart;
import ru.ulstu.model.shop.Order;
import ru.ulstu.model.shop.Product;
import ru.ulstu.model.shop.User;
import ru.ulstu.model.transport.company.Deliver;
import ru.ulstu.model.transport.company.Driver;
import ru.ulstu.model.transport.company.Transport;
import ru.ulstu.service.CrudService;

import java.time.Instant;
import java.util.HashSet;

@SpringBootApplication
public class ExampleJpaApplication {
    private static final Logger log = LoggerFactory.getLogger(ExampleJpaApplication.class);

    private final CrudService crudService;

    public ExampleJpaApplication(CrudService crudService) {
        this.crudService = crudService;
    }

    public static void main(String[] args) {
        SpringApplication.run(ExampleJpaApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void onStart() {
//        for (int i = 0; i < 1000; i++) {
//            User user = crudService.createUser("Name" + i, "Name" + i, "Address" + i, "email@" + i, "" + i);
//            Product product = crudService.createProduct("Title" + i, i, "Description" + i);
//            Cart cart = crudService.createCart(user, product, i, "promo" + i);
//            Driver driver = crudService.createDriver("Name" + i * 10, "Name" + i * 10, "passport" + i);
//            Transport transport = crudService.createTransport(driver, "Number" + i, "Model" + i);
//            Deliver deliver = crudService.createDeliver(transport, Instant.now().toEpochMilli(), Instant.now().toEpochMilli() + 180000, "address" + i, "address" + i * 20);
//            Order order = crudService.createOrder(Instant.now().toEpochMilli(), Instant.now().toEpochMilli(), "address" + i, true, deliver);
//            cart.setOrder(order);
//            crudService.updateCart(cart);
//        }

        long t = Instant.now().toEpochMilli();
        crudService.showAllRecords();
        long allTime = Instant.now().toEpochMilli() - t;
        t = Instant.now().toEpochMilli();
        crudService.showFirstPageOfRecords();
        long firstTime = Instant.now().toEpochMilli() - t;
        t = Instant.now().toEpochMilli();
        crudService.showFilteredRecords();
        long filterTime = Instant.now().toEpochMilli() - t;
        t = Instant.now().toEpochMilli();
        crudService.showStatistic();
        long statsTime = Instant.now().toEpochMilli() - t;

        log.warn("showAllRecords func ends for " + allTime + " millis");
        log.warn("showFirstPageOfRecords func ends for " + firstTime + " millis");
        log.warn("showFilteredRecords func ends for " + filterTime + " millis");
        log.warn("showStatistic func ends for " + statsTime + " millis");
    }
}
