package ru.ulstu.repository.transport.company;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.model.transport.company.Transport;

public interface TransportRepository extends JpaRepository<Transport, Integer> {
}
