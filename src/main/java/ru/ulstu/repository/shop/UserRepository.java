package ru.ulstu.repository.shop;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.model.shop.User;

public interface UserRepository extends JpaRepository<User, Integer> {
}
