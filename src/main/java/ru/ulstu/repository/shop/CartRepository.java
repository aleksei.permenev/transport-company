package ru.ulstu.repository.shop;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.model.shop.Cart;

public interface CartRepository extends JpaRepository<Cart, Integer> {
}
